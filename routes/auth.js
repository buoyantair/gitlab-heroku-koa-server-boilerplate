const Router = require('koa-router')
const jwt = require('jsonwebtoken')

const { TOKEN_SECRET } = process.env

const authRouter = new Router()

authRouter.post('/auth', async ctx => {
  const { username, password } = ctx.request.body

  if (!username) { ctx.throw(400, 'username is required') }
  if (!password) { ctx.throw(400, 'password is required') }

  ctx.body = {
    token: await createToken(username, password, TOKEN_SECRET, '30m')
  }
})

async function createToken (username, password, secret, expiresIn) {
  return jwt.sign({
    username,
    password
  }, secret, { expiresIn })
}

module.exports = authRouter
