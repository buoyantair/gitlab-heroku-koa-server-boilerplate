# Boilerplate for koajs node server

This boilerplate contains the following features:

- Typescript support
- Gitlab CI integration
- Automated Gitlab to Heroku deployment (CD / Continous Deployment) (via [dpl](https://github.com/travis-ci/dpl#heroku))
- Unit & integration testing with jest & supertest


## Contributing

Contributions are welcome! Feel free to fork the repo and send PRs!